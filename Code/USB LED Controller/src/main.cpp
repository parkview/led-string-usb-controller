#include <Arduino.h>
#include "FastLED.h"
#include <MPU6050_tockn.h>
#include <Wire.h>

const uint8_t I2C_SDA_PIN = 22; //SDA;  // ESP32 WROOM32 i2c SDA Pin
const uint8_t I2C_SCL_PIN = 21; //SCL;  // ESP32 WROOM32 i2c SCL Pin
#define LEDUSER1  26  // GPIO of the Red D1
#define LEDUSER2  27  // GPIO of the Green D2
#define LEDUSER3  32  // GPIO of the Red D3
#define LEDUSER4  33  // GPIO of the Green D4
#define USBLED1  12   // GPIO of the USB1 power out
#define USBLED2  13   // GPIO of the USB2 power out
#define USBLED3  5    // GPIO of the USB3 power out
#define RGBLED   19  // GPIO of the RGB WS2812 Data output
#define Button1  34  // SW1 - press = 0
#define Button2  35  // SW2 - press = 0
void sequencePulseLEDs(); 

#define numRGBLED  30   // number of RGB LEDs to light up
#define BRIGHTNESS 228   /* Control the brightness of your leds */
#define SATURATION 255   /* Control the saturation of your leds */
#define WAIT  100
#define DELAYVAL 50
#define Current4  39
const int freq = 5000;
const int ledChannel1 = 0;
const int ledChannel2 = 1;
const int ledChannel3 = 2;
const int resolution1 = 8;
const int resolution2 = 8;
const int resolution3 = 8;
MPU6050 mpu6050(Wire);
long timer = 0;
CRGB leds[numRGBLED];

float truncate(float val, byte dec)
// truncate a float value to dec number of digits
{
    float x = val * pow(10, dec);
    float y = round(x);
    float z = x - y;
    if ((int)z == 5)
    {
        y++;
    } else {}
    x = y / pow(10, dec);
    return x;
}

float readCurrent1(void) {
    // reads the raw ADC input of the USB LED board voltage and converts it to a formated current reading
    long CurrentRaw = 0UL;
    float currentAvg= 0.0;
    //float resistor33Converstion = 19942.2;
    float resistor66Converstion = 9971.1;
    //float resistor10Converstion = 73314.0;
    int avgNumber = 10;

    for (int i = 0; i < avgNumber; i++)  // read a number of times to average out noise
    {
        CurrentRaw += analogRead(Current4);
        //Serial.println(CurrentRaw);
        delay(1);
    }
    if (CurrentRaw == 4095)
    { CurrentRaw = 0;
    }
    currentAvg = truncate(CurrentRaw/resistor66Converstion, 3);  // convert to real voltage and truncate float to 2 digits
    return currentAvg;
}

void rainbow_wave() {
    for (int j = 0; j < 255; j++) {
    for (int i = 0; i < numRGBLED; i++) {
      leds[i] = CHSV(i - (j * 2), BRIGHTNESS, SATURATION); /* The higher the value 4 the less fade there is and vice versa */ 
    }
    FastLED.show();
    delay(125); /* Change this to your hearts desire, the lower the value the faster your colors move (and vice versa) */
  }
}
void rainbow(uint8_t thisSpeed, uint8_t deltaHue) {     // The fill_rainbow call doesn't support brightness levels.
// uint8_t thisHue = beatsin8(thisSpeed,0,255);                // A simple rainbow wave.
 uint8_t thisHue = beat8(thisSpeed,255);                     // A simple rainbow march.
 fill_rainbow(leds, numRGBLED, thisHue, deltaHue);            // Use FastLED's fill_rainbow routine.
} // rainbow_wave()

void LEDPWM(int ledchannel, int LEDdelay) {
    // increase the LED brightness
  for(int dutyCycle = 0; dutyCycle <= 255; dutyCycle++){   
    // changing the LED brightness with PWM
    ledcWrite(ledchannel, dutyCycle);
    delay(LEDdelay);
  }
  //Serial.println("A: " + String(readCurrent1()));
  // decrease the LED brightness
  for(int dutyCycle = 255; dutyCycle >= 0; dutyCycle--){
    // changing the LED brightness with PWM
    ledcWrite(ledchannel, dutyCycle);   
    delay(LEDdelay);
  }
}

void blinkUserLED() {
  // blink the User LEDs, Red, then Green
digitalWrite(LEDUSER1, HIGH);
digitalWrite(LEDUSER3, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER1, LOW);
digitalWrite(LEDUSER3, LOW);
delay(WAIT);
digitalWrite(LEDUSER2, HIGH);
digitalWrite(LEDUSER4, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER2, LOW);
digitalWrite(LEDUSER4, LOW);
}

void blinkUserLEDGreen() {
  // blink both the Green User LEDs
digitalWrite(LEDUSER2, HIGH);
digitalWrite(LEDUSER4, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER2, LOW);
digitalWrite(LEDUSER4, LOW);
}

void blinkUserLEDRed() {
  // blink both the Green User LEDs
digitalWrite(LEDUSER1, HIGH);
digitalWrite(LEDUSER3, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER1, LOW);
digitalWrite(LEDUSER3, LOW);
}

void LEDDisplay(int channel, float dutyCycle, float dcOld) {
  // this will display required brightness according to MPU
  float diff;
  int pause = 50;
  diff = (dutyCycle - dcOld);
  //Serial.println(dutyCycle + "\t" + dcOld + "\t" + diff);
  if (abs(diff) > 6) {
    // too big a difference.  Split it up and display differences as stepped flashes
  Serial.print(dutyCycle); Serial.print("\t"+String (dcOld)); Serial.println("\t"+ String (diff));
    diff = diff / 3;
    dcOld = dcOld + diff;
    Serial.print(channel);Serial.println("\t"+String(dcOld));
    ledcWrite(channel, dcOld);
    delay(pause);
    dcOld = dcOld + diff;
    Serial.print(channel);Serial.println("\t"+String(dcOld));
    ledcWrite(channel, dcOld);
    delay(pause);
    dcOld = dcOld + diff;
    Serial.print(channel);Serial.println("\t"+String(dcOld));
    ledcWrite(channel, dcOld);
  } else {
    ledcWrite(channel, dutyCycle);
  }

}
void accLEDPort() {
  // will work out the optimum way to get to the new angle and change the LED light level to match
  float dutyCycleX = 0.0;
  float dutyCycleY = 0.0;
  float dutyCycleZ = 0.0;
  float dcOldX = 0.0;
  float dcOldY = 0.0;
  float dcOldZ = 0.0;
  float AccX = 0.0;
  float AccY = 0.0;
  float AccZ = 0.0;
    while (1) {
    mpu6050.update();
    AccX = mpu6050.getAccX();
    AccY = mpu6050.getAccY();
    AccZ = mpu6050.getAccZ();
    //Serial.print("temp : ");Serial.println(mpu6050.getTemp());
    dutyCycleX = abs(AccX) * 255;
    dutyCycleY = abs(AccY) * 255;
    dutyCycleZ = abs(AccZ) * 255;
    if (dutyCycleX > 255) { dutyCycleX = 255;}
    if (dutyCycleY > 255) { dutyCycleY = 255;}
    if (dutyCycleZ > 255) { dutyCycleZ = 255;}
 /*   
    Serial.println("=======================================================");
    Serial.print("accX : ");Serial.print(AccX); 
    Serial.print("\taccY : ");Serial.print(AccY); 
    Serial.print("\taccZ : ");Serial.println(AccZ); 
    Serial.print("dcX: ");Serial.print(dutyCycleX);
    Serial.print("\tdcY: ");Serial.print(dutyCycleY);
    Serial.print("\tdcZ: ");Serial.println(dutyCycleZ);
*/
    //Serial.println("A: " + String(readCurrent1()) + "A");
    LEDDisplay(0, dutyCycleX, dcOldX);
    LEDDisplay(1, dutyCycleY, dcOldY);
    LEDDisplay(2, dutyCycleZ, dcOldZ);
    dcOldX = dutyCycleX;
    dcOldY = dutyCycleY;
    dcOldZ = dutyCycleZ;
    //delay(WAIT);
    if (digitalRead(Button1) == 0 ) {
      sequencePulseLEDs();
    }
  } 
}

void sequencePulseLEDs() {
  // pulse the LED strings in sequence
  blinkUserLEDGreen();
  while (1) {
    LEDPWM(ledChannel1, random(1,10));
    LEDPWM(ledChannel2, random(1,10));
    LEDPWM(ledChannel3, random(1,10));
    if (digitalRead(Button2) == 0 ) {
      accLEDPort();
    }
  }
}

void fluidPulseLEDs() {
  // pulse the LEDs strings all intermingled
  blinkUserLEDRed();
  while (1) {


    if (digitalRead(Button1) == 0 || digitalRead(Button2) == 0) {
      while (digitalRead(Button1) == 0 || digitalRead(Button2) == 0) {
        if (digitalRead(Button1 == 0)) {
          digitalWrite(LEDUSER2, HIGH);
        }
        if (digitalRead(Button2 == 0)) {
          digitalWrite(LEDUSER4, HIGH);
        }
      digitalWrite(LEDUSER2, LOW);
      digitalWrite(LEDUSER4, LOW);
      break; }
    }
  }
}
void setup() {
  // Setup ESP32 GPIOs:
  pinMode(LEDUSER1, OUTPUT);
  pinMode(LEDUSER2, OUTPUT);
  pinMode(LEDUSER3, OUTPUT);
  pinMode(LEDUSER4, OUTPUT);
  pinMode(Button1, INPUT);
  pinMode(Button2, INPUT);
  //pinMode(USBLED1, OUTPUT);
  //pinMode(USBLED2, OUTPUT);
  //pinMode(USBLED3, OUTPUT);
  pinMode(RGBLED, OUTPUT);
  FastLED.addLeds<NEOPIXEL, RGBLED>(leds, numRGBLED);
//   FastLED.addLeds<WS2812B, RGBLED, RGB>(leds, numRGBLED);  // GRB ordering is typical
  blinkUserLED();
  Serial.begin(921600);
  Wire.begin(22,21);
  Wire.beginTransmission(0x69); // Begins a transmission to the I2C slave (GY-521 board)
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
  ledcSetup(ledChannel1, freq, resolution1);
  ledcSetup(ledChannel2, freq, resolution2);
  ledcSetup(ledChannel3, freq, resolution3);
  ledcAttachPin(USBLED1, ledChannel1);
  ledcAttachPin(USBLED2, ledChannel2);
  ledcAttachPin(USBLED3, ledChannel3);
}

void loop() {
  // test for button presses.  Run code depending on whats pressed
  blinkUserLEDGreen();
 	leds[0] = CRGB::Red; FastLED.show(); delay(300);
 	leds[1] = CRGB::Green; FastLED.show(); delay(300);
 	leds[2] = CRGB::Blue; FastLED.show(); delay(300);
 	leds[3] = CRGB::White; FastLED.show(); delay(300);
	leds[0] = CRGB::Black; FastLED.show(); ;
  rainbow(10, 10); // Speed, delta hue values.
  FastLED.show();
  rainbow_wave();  // dynamic wave of color
  delay(WAIT);
  delay(WAIT);
  if (digitalRead(Button1) == 0) {
    // go and Pulse LEDs in sequence
    sequencePulseLEDs();
  }
  if (digitalRead(Button2) == 0) {
    // go and Pulse LEDs - all intermingled
    //fluidPulseLEDs();
    accLEDPort();
  }
  // no button pressed, so run the default Gravity LED program
  accLEDPort();
  //delay(WAIT);
}