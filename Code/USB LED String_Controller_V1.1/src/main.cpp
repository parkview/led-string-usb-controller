#include <Arduino.h>
#include "FastLED.h"
#include <MPU6050_tockn.h>
#include <Wire.h>
#include "SSD1306Wire.h"
#include <Adafruit_INA219.h>
#include <Adafruit_Sensor.h>
#include "Adafruit_TSL2591.h"

const uint8_t I2C_SDA_PIN = 22; //SDA;  // ESP32 WROOM32 i2c SDA Pin
const uint8_t I2C_SCL_PIN = 21; //SCL;  // ESP32 WROOM32 i2c SCL Pin
#define LEDUSER1  32   // GPIO of the Red D1
#define LEDUSER2  33   // GPIO of the Green D2
#define LEDUSER3  26   // GPIO of the Red D3
#define LEDUSER4  27   // GPIO of the Green D4
#define USBLED1  13    // GPIO of the USB1 power out
#define USBLED2   5    // GPIO of the USB2 power out
#define USBLED3  15    // GPIO of the USB3 power out
#define RGBLED   19    // GPIO of the RGB WS2812 Data output
#define RGBLED2  18    // GPIO of the RGB WS2812 Data output
#define MOTOR1   16    // GPIO of the RGB WS2812 Data output
#define Button1  34    // SW1 - press = 0
#define Button2  35    // SW2 - press = 0
void sequencePulseLEDs(); 

#define numRGBLED  30   // number of RGB LEDs to light up
#define BRIGHTNESS 228   /* Control the brightness of your leds */
#define SATURATION 255   /* Control the saturation of your leds */
#define WAIT  100
#define DELAYVAL 50
#define Current4  39
const int freq = 5000;
const int ledChannel1 = 0;
const int ledChannel2 = 1;
const int ledChannel3 = 2;
const int resolution1 = 8;
const int resolution2 = 8;
const int resolution3 = 8;
const int motorPin = 16;
const uint8_t DISPLAY_WIDTH = 128;  // OLED display pixel width
const uint8_t DISPLAY_HEIGHT = 64;  // OLED display pixel hight
#define OLED_DISPLAY true    // Enable OLED Display
#define OLED_DISPLAY_SSD1306  // OLED Display Type: SSD1306(OLED_DISPLAY_SSD1306) / SH1106(OLED_DISPLAY_SH1106), comment this line out to disable oled
MPU6050 mpu6050(Wire);
long timer = 0;
CRGB leds[numRGBLED];
SSD1306Wire  display(0x3c,I2C_SDA_PIN, I2C_SCL_PIN);
Adafruit_INA219 ina219(0x44);   // PCB i2C address is 0x44
Adafruit_TSL2591 tsl = Adafruit_TSL2591(2591); // pass in a number for the sensor identifier (for your use later)

// pre-list some funcations ahead of use
float readCurrent(void) ;
float readVoltage(void);
void displayText(int, char*);

void displaySensorDetails(void)
{
  sensor_t sensor;
  tsl.getSensor(&sensor);
  Serial.println(F("------------------------------------"));
  Serial.print  (F("Sensor:       ")); Serial.println(sensor.name);
  Serial.print  (F("Driver Ver:   ")); Serial.println(sensor.version);
  Serial.print  (F("Unique ID:    ")); Serial.println(sensor.sensor_id);
  Serial.print  (F("Max Value:    ")); Serial.print(sensor.max_value); Serial.println(F(" lux"));
  Serial.print  (F("Min Value:    ")); Serial.print(sensor.min_value); Serial.println(F(" lux"));
  Serial.print  (F("Resolution:   ")); Serial.print(sensor.resolution, 4); Serial.println(F(" lux"));  
  Serial.println(F("------------------------------------"));
  Serial.println(F(""));
  delay(500);
}

void configureSensor(void)
{
  // You can change the gain on the fly, to adapt to brighter/dimmer light situations
  //tsl.setGain(TSL2591_GAIN_LOW);    // 1x gain (bright light)
  tsl.setGain(TSL2591_GAIN_MED);      // 25x gain
  //tsl.setGain(TSL2591_GAIN_HIGH);   // 428x gain
  
  // Changing the integration time gives you a longer time over which to sense light
  // longer timelines are slower, but are good in very low light situtations!
  //tsl.setTiming(TSL2591_INTEGRATIONTIME_100MS);  // shortest integration time (bright light)
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_200MS);
  tsl.setTiming(TSL2591_INTEGRATIONTIME_300MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_400MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_500MS);
  // tsl.setTiming(TSL2591_INTEGRATIONTIME_600MS);  // longest integration time (dim light)

  /* Display the gain and integration time for reference sake */  
  Serial.println(F("------------------------------------"));
  Serial.print  (F("Gain:         "));
  tsl2591Gain_t gain = tsl.getGain();
  switch(gain)
  {
    case TSL2591_GAIN_LOW:
      Serial.println(F("1x (Low)"));
      break;
    case TSL2591_GAIN_MED:
      Serial.println(F("25x (Medium)"));
      break;
    case TSL2591_GAIN_HIGH:
      Serial.println(F("428x (High)"));
      break;
    case TSL2591_GAIN_MAX:
      Serial.println(F("9876x (Max)"));
      break;
  }
  Serial.print  (F("Timing:       "));
  Serial.print((tsl.getTiming() + 1) * 100, DEC); 
  Serial.println(F(" ms"));
  Serial.println(F("------------------------------------"));
  Serial.println(F(""));
}

void simpleRead(void)
{
  // Simple data read example. Just read the infrared, fullspecrtrum diode 
  // or 'visible' (difference between the two) channels.
  // This can take 100-600 milliseconds! Uncomment whichever of the following you want to read
  uint16_t x = tsl.getLuminosity(TSL2591_VISIBLE);
  //uint16_t x = tsl.getLuminosity(TSL2591_FULLSPECTRUM);
  //uint16_t x = tsl.getLuminosity(TSL2591_INFRARED);

  Serial.print(F("[ ")); Serial.print(millis()); Serial.print(F(" ms ] "));
  Serial.print(F("Luminosity: "));
  Serial.println(x, DEC);
  char msg1[7] = {"......"};
  sprintf(msg1, "Lux: %d\n\r\0", x);   // TODO: cant get this to terminate properly
  displayText(2, msg1);
}

/**************************************************************************/
/*
    Show how to read IR and Full Spectrum at once and convert to lux
*/
/**************************************************************************/
void advancedRead(void)
{
  // More advanced data read example. Read 32 bits with top 16 bits IR, bottom 16 bits full spectrum
  // That way you can do whatever math and comparisons you want!
  uint32_t lum = tsl.getFullLuminosity();
  uint16_t ir, full;
  ir = lum >> 16;
  full = lum & 0xFFFF;
  Serial.print(F("[ ")); Serial.print(millis()); Serial.print(F(" ms ] "));
  Serial.print(F("IR: ")); Serial.print(ir);  Serial.print(F("  "));
  Serial.print(F("Full: ")); Serial.print(full); Serial.print(F("  "));
  Serial.print(F("Visible: ")); Serial.print(full - ir); Serial.print(F("  "));
  Serial.print(F("Lux: ")); Serial.println(tsl.calculateLux(full, ir), 6);
}

/**************************************************************************/
/*
    Performs a read using the Adafruit Unified Sensor API.
*/
/**************************************************************************/
void unifiedSensorAPIRead(void)
{
  /* Get a new sensor event */ 
  sensors_event_t event;
  tsl.getEvent(&event);
 
  /* Display the results (light is measured in lux) */
  Serial.print(F("[ ")); Serial.print(event.timestamp); Serial.print(F(" ms ] "));
  if ((event.light == 0) |
      (event.light > 4294966000.0) | 
      (event.light <-4294966000.0))
  {
    /* If event.light = 0 lux the sensor is probably saturated */
    /* and no reliable data could be generated! */
    /* if event.light is +/- 4294967040 there was a float over/underflow */
    Serial.println(F("Invalid data (adjust gain or timing)"));
  }
  else
  {
    Serial.print(event.light); Serial.println(F(" lux"));
  }
}

void OLED_Header() {
  char msg[18];
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.drawString(0, 0, "USB LEDs");
  display.drawLine(0, 12, DISPLAY_WIDTH, 12);
  display.setTextAlignment(TEXT_ALIGN_RIGHT);
  //display.setTextAlignment(TEXT_ALIGN_LEFT);
  //sprintf(msg, "V:%.1f   A:%.3f\0", readVoltage(),readCurrent()/100 );
  sprintf(msg, "V:%.1f  mA:%.0f", readVoltage(),readCurrent() );
  display.drawString(128, 0, msg);   // currently have a 0.1 sensor resistor in place, so divide by 100
  display.setTextAlignment(TEXT_ALIGN_LEFT);
  display.display();
}

void testAcc() {
  // will work out the optimum way to get to the new angle and change the LED light level to match
  float dutyCycleX = 0.0;
  float dutyCycleY = 0.0;
  float dutyCycleZ = 0.0;
  float dcOldX = 0.0;
  float dcOldY = 0.0;
  float dcOldZ = 0.0;
  float AccX = 0.0;
  float AccY = 0.0;
  float AccZ = 0.0;
    while (1) {
    mpu6050.update();
    AccX = mpu6050.getAccX();
    AccY = mpu6050.getAccY();
    AccZ = mpu6050.getAccZ();
    //Serial.print("temp : ");Serial.println(mpu6050.getTemp());
    dutyCycleX = abs(AccX) * 255;
    dutyCycleY = abs(AccY) * 255;
    dutyCycleZ = abs(AccZ) * 255;
    if (dutyCycleX > 255) { dutyCycleX = 255;}
    if (dutyCycleY > 255) { dutyCycleY = 255;}
    if (dutyCycleZ > 255) { dutyCycleZ = 255;}
    
    Serial.println("=======================================================");
    Serial.print("accX : ");Serial.print(AccX); 
    Serial.print("\taccY : ");Serial.print(AccY); 
    Serial.print("\taccZ : ");Serial.println(AccZ); 
    Serial.print("dcX: ");Serial.print(dutyCycleX);
    Serial.print("\tdcY: ");Serial.print(dutyCycleY);
    Serial.print("\tdcZ: ");Serial.println(dutyCycleZ);
    display.clear();
    OLED_Header();
    display.drawString(2, 12, "X: " + String(AccX));
    display.drawString(2, 22, "Y: " + String(AccY));
    display.drawString(2, 32, "Z: " + String(AccZ));
    display.display();
    dcOldX = dutyCycleX;
    dcOldY = dutyCycleY;
    dcOldZ = dutyCycleZ;
    //delay(WAIT);
  } 
}

float readCurrent(void) {
  // Read current from INA219.
  float current_mA = ina219.getCurrent_mA() ;
  return current_mA;
}

float readVoltage(void) {
  // Read voltage from INA219.
  float shuntvoltage = ina219.getShuntVoltage_mV();
  float busvoltage = ina219.getBusVoltage_V();
  float loadvoltage = busvoltage + (shuntvoltage / 999);
  return loadvoltage;
}

void displayText(int line, char *textline) {
  // test out the INA219 current/voltage sensor
  display.clear();
  OLED_Header();
  display.drawString(2, line * 12, textline); 
  delay(300);
  display.display();
}

float truncate(float val, byte dec)
// truncate a float value to dec number of digits
{
    float x = val * pow(10, dec);
    float y = round(x);
    float z = x - y;
    if ((int)z == 5)
    {
        y++;
    } else {}
    x = y / pow(10, dec);
    return x;
}

float readCurrent1(void) {
    // reads the raw ADC input of the USB LED board voltage and converts it to a formated current reading
    long CurrentRaw = 0UL;
    float currentAvg= 0.0;
    //float resistor33Converstion = 19942.2;
    float resistor66Converstion = 9971.1;
    //float resistor10Converstion = 73314.0;
    int avgNumber = 10;

    for (int i = 0; i < avgNumber; i++)  // read a number of times to average out noise
    {
        CurrentRaw += analogRead(Current4);
        //Serial.println(CurrentRaw);
        delay(1);
    }
    if (CurrentRaw == 4095)
    { CurrentRaw = 0;
    }
    currentAvg = truncate(CurrentRaw/resistor66Converstion, 3);  // convert to real voltage and truncate float to 2 digits
    return currentAvg;
}

void rainbow_wave() {
    for (int j = 0; j < 255; j++) {
    for (int i = 0; i < numRGBLED; i++) {
      leds[i] = CHSV(i - (j * 2), BRIGHTNESS, SATURATION); /* The higher the value 4 the less fade there is and vice versa */ 
    }
    FastLED.show();
    delay(125); /* Change this to your hearts desire, the lower the value the faster your colors move (and vice versa) */
  }
}
void rainbow(uint8_t thisSpeed, uint8_t deltaHue) {     // The fill_rainbow call doesn't support brightness levels.
// uint8_t thisHue = beatsin8(thisSpeed,0,255);                // A simple rainbow wave.
 uint8_t thisHue = beat8(thisSpeed,255);                     // A simple rainbow march.
 fill_rainbow(leds, numRGBLED, thisHue, deltaHue);            // Use FastLED's fill_rainbow routine.
} // rainbow_wave()

void LEDPWM(int ledchannel, int LEDdelay) {
    // increase the LED brightness
  for(int dutyCycle = 0; dutyCycle <= 255; dutyCycle++){   
    // changing the LED brightness with PWM
    ledcWrite(ledchannel, dutyCycle);
    delay(LEDdelay);
  }
  //Serial.println("A: " + String(readCurrent1()));
  // decrease the LED brightness
  for(int dutyCycle = 255; dutyCycle >= 0; dutyCycle--){
    // changing the LED brightness with PWM
    ledcWrite(ledchannel, dutyCycle);   
    delay(LEDdelay);
  }
}

void blinkUserLED() {
  // blink the User LEDs, Red, then Green
digitalWrite(LEDUSER1, HIGH);
digitalWrite(LEDUSER3, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER1, LOW);
digitalWrite(LEDUSER3, LOW);
delay(WAIT);
digitalWrite(LEDUSER2, HIGH);
digitalWrite(LEDUSER4, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER2, LOW);
digitalWrite(LEDUSER4, LOW);
}

void blinkUserLEDGreen() {
  // blink both the Green User LEDs
digitalWrite(LEDUSER2, HIGH);
digitalWrite(LEDUSER4, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER2, LOW);
digitalWrite(LEDUSER4, LOW);
}

void blinkUserLEDRed() {
  // blink both the Green User LEDs
digitalWrite(LEDUSER1, HIGH);
digitalWrite(LEDUSER3, HIGH);
delay(WAIT);
delay(WAIT);
digitalWrite(LEDUSER1, LOW);
digitalWrite(LEDUSER3, LOW);
}

void LEDDisplay(int channel, float dutyCycle, float dcOld) {
  // this will display required brightness according to MPU
  float diff;
  int pause = 50;
  diff = (dutyCycle - dcOld);
  //Serial.println(dutyCycle + "\t" + dcOld + "\t" + diff);
  if (abs(diff) > 6) {
    // too big a difference.  Split it up and display differences as stepped flashes
  Serial.print(dutyCycle); Serial.print("\t"+String (dcOld)); Serial.println("\t"+ String (diff));
    diff = diff / 3;
    dcOld = dcOld + diff;
    Serial.print(channel);Serial.println("\t"+String(dcOld));
    ledcWrite(channel, dcOld);
    delay(pause);
    dcOld = dcOld + diff;
    Serial.print(channel);Serial.println("\t"+String(dcOld));
    ledcWrite(channel, dcOld);
    delay(pause);
    dcOld = dcOld + diff;
    Serial.print(channel);Serial.println("\t"+String(dcOld));
    ledcWrite(channel, dcOld);
  } else {
    ledcWrite(channel, dutyCycle);
  }

}
void accLEDPort() {
  // will work out the optimum way to get to the new angle and change the LED light level to match
  float dutyCycleX = 0.0;
  float dutyCycleY = 0.0;
  float dutyCycleZ = 0.0;
  float dcOldX = 0.0;
  float dcOldY = 0.0;
  float dcOldZ = 0.0;
  float AccX = 0.0;
  float AccY = 0.0;
  float AccZ = 0.0;
    while (1) {
    mpu6050.update();
    AccX = mpu6050.getAccX();
    AccY = mpu6050.getAccY();
    AccZ = mpu6050.getAccZ();
    Serial.print("temp : ");Serial.println(mpu6050.getTemp());
    dutyCycleX = abs(AccX) * 255;
    dutyCycleY = abs(AccY) * 255;
    dutyCycleZ = abs(AccZ) * 255;
    if (dutyCycleX > 255) { dutyCycleX = 255;}
    if (dutyCycleY > 255) { dutyCycleY = 255;}
    if (dutyCycleZ > 255) { dutyCycleZ = 255;}
 /*   
    Serial.println("=======================================================");
    Serial.print("accX : ");Serial.print(AccX); 
    Serial.print("\taccY : ");Serial.print(AccY); 
    Serial.print("\taccZ : ");Serial.println(AccZ); 
    Serial.print("dcX: ");Serial.print(dutyCycleX);
    Serial.print("\tdcY: ");Serial.print(dutyCycleY);
    Serial.print("\tdcZ: ");Serial.println(dutyCycleZ);
*/
    Serial.println("A: " + String(readCurrent1()) + "A");
    LEDDisplay(0, dutyCycleX, dcOldX);
    LEDDisplay(1, dutyCycleY, dcOldY);
    LEDDisplay(2, dutyCycleZ, dcOldZ);
    dcOldX = dutyCycleX;
    dcOldY = dutyCycleY;
    dcOldZ = dutyCycleZ;
    //delay(WAIT);
    if (digitalRead(Button1) == LOW ) {
      sequencePulseLEDs();
    }
  } 
}

void sequencePulseLEDs() {
  // pulse the LED strings in sequence
  blinkUserLEDGreen();
  while (1) {
    LEDPWM(ledChannel1, random(1,10));
    LEDPWM(ledChannel2, random(1,10));
    LEDPWM(ledChannel3, random(1,10));
    if (digitalRead(Button2) == LOW ) {
      accLEDPort();
    }
  }
}

void fluidPulseLEDs() {
  // pulse the LEDs strings all intermingled
  blinkUserLEDRed();
  while (1) {

// 2021-01-01:  changed button == 0 to LOW
    if (digitalRead(Button1) == LOW || digitalRead(Button2) == LOW) {
      while (digitalRead(Button1) == LOW || digitalRead(Button2) == LOW) {
        if (digitalRead(Button1) == LOW) {
          digitalWrite(LEDUSER2, HIGH);
        }
        if (digitalRead(Button2) == LOW) {
          digitalWrite(LEDUSER4, HIGH);
        }
      digitalWrite(LEDUSER2, LOW);
      digitalWrite(LEDUSER4, LOW);
      break; }
    }
  }
}

void testINA219() {
  // test out the INA219 - is it responding at i2C address 0x44, with voltage and current
  float amps = readCurrent();
  float volts = readVoltage();
  char reg1[10];
  char reg2[10];
  sprintf(reg1, "mA: %0.1f",amps );
  sprintf(reg2, "V: %0.1f",volts);
  displayText(3, reg2);
  delay(200);
  displayText(2, reg1);
  delay(200);
  Serial.println(String(ina219.getBusVoltage_V()));
  Serial.println(String(ina219.getShuntVoltage_mV()));
  Serial.println(String(ina219.getCurrent_mA()));
  delay(200);
  //Serial.println(String(readVoltage()));
  /*
  Serial.print("Volts:");
  Serial.println(reg2);
  Serial.print("mA:");
  Serial.println(reg1);
*/
}

void testVMotor(int motorStatus) {
  // turn motor on/off on GPIO16
  if (motorStatus == 0) {
    digitalWrite(motorPin, LOW);
    //displayText(4, "Motor: Off");
    Serial.println("Motor: Off");
  }
  else {
    digitalWrite(motorPin, HIGH);
    //displayText(4, "Motor: On");
    Serial.println("Motor: On");
  }
}

void setup() {
  // Setup ESP32 GPIOs:
  pinMode(LEDUSER1, OUTPUT);
  pinMode(LEDUSER2, OUTPUT);
  pinMode(LEDUSER3, OUTPUT);
  pinMode(LEDUSER4, OUTPUT);
  pinMode(motorPin, OUTPUT);
  pinMode(Button1, INPUT);
  pinMode(Button2, INPUT);
  //pinMode(USBLED1, OUTPUT);
  //pinMode(USBLED2, OUTPUT);
  //pinMode(USBLED3, OUTPUT);
  pinMode(RGBLED, OUTPUT);
  testVMotor(0);   // make sure motor is switched off
  FastLED.addLeds<NEOPIXEL, RGBLED>(leds, numRGBLED);
//   FastLED.addLeds<WS2812B, RGBLED, RGB>(leds, numRGBLED);  // GRB ordering is typical
  blinkUserLED();
  Serial.begin(115200);
  Wire.begin(22,21);   // (pin_SDA, pin_SLC)
  Wire.beginTransmission(0x69); // Begins a transmission to the I2C slave MPU-6050
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
  ledcSetup(ledChannel1, freq, resolution1);
  ledcSetup(ledChannel2, freq, resolution2);
  ledcSetup(ledChannel3, freq, resolution3);
  ledcAttachPin(USBLED1, ledChannel1);
  ledcAttachPin(USBLED2, ledChannel2);
  ledcAttachPin(USBLED3, ledChannel3);
  // turn off the string leds
  ledcWrite(1, 0);
  ledcWrite(2, 0);
  ledcWrite(3, 0);
  
  #if OLED_DISPLAY
    display.init();
    display.flipScreenVertically();   // setup depends on how the PCB is mounted
    display.setContrast(255);
    display.setFont(ArialMT_Plain_10);
    display.setColor(WHITE);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 0, "USB LEDs");
    display.drawLine(0, 12, DISPLAY_WIDTH, 12);
    display.display();
  #endif
  if (tsl.begin()) 
  {
    Serial.println(F("Found a TSL2591 sensor"));
  } 
  else 
  {
    Serial.println(F("No sensor found ... check your wiring?"));
    while (1);
  }
   /* Display some basic information on this sensor */
  displaySensorDetails();
  /* Configure the sensor */
  configureSensor();
  if (! ina219.begin()) {
    Serial.println("Failed to find INA219 chip at 0x44 !!!!!");
    //while (1) { delay(10); }
  }
  // ina219.setCalibration_16V_400mA();  // make readings more sensitive
}

void loop() {
  // test for button presses.  Run code depending on whats pressed
  blinkUserLEDGreen();
  /*
 	leds[0] = CRGB::Red; FastLED.show(); delay(300);
 	leds[1] = CRGB::Green; FastLED.show(); delay(300);
 	leds[2] = CRGB::Blue; FastLED.show(); delay(300);
 	leds[3] = CRGB::White; FastLED.show(); delay(300);
	leds[0] = CRGB::Black; FastLED.show(); 
  Serial.println("single leds finished");
  rainbow(10, 10); // Speed, delta hue values.
  FastLED.show();
  Serial.println("Rainbow 10-10 finished");
  FastLED.clear();  // clear all pixel data
  FastLED.show();
  */
  testAcc();   // TODO: MPU-6050, reading zero
  //testINA219();
  // simpleRead();     // just read the LUX level 
  // advancedRead();   // go read the TSL2591 LUX sensor
  // unifiedSensorAPIRead();
  /* 
  testVMotor(1);   // turn on haptic motor
  delay(2000);
  testVMotor(0);   // turn off haptic motor
  delay(2000);
  */
  // sequencePulseLEDs(); 
  /*
  blinkUserLEDGreen();
  rainbow_wave();  // dynamic wave of color
	leds[0][30] = CRGB::Black; FastLED.show(); 
  Serial.println("Rainbow wave finished");
  blinkUserLEDGreen();
  delay(WAIT);
  delay(WAIT);
  FastLED.clear();  // clear all pixel data
  FastLED.show();
  if (digitalRead(Button1) == LOW) {
    // go and Pulse LEDs in sequence
  Serial.println("SW1 pressed");
  displayText(3, "SW1 pressed");
    sequencePulseLEDs();
  }
  if (digitalRead(Button2) == LOW) {
    // go and Pulse LEDs - all intermingled
    //fluidPulseLEDs();
  Serial.println("SW2 pressed");
  displayText(3, "SW2 pressed");
    accLEDPort();
  }
  // no button pressed, so run the default Gravity LED program
  FastLED.clear();  // clear all pixel data
  FastLED.show();
  Serial.println("Go run accLEDPort");
  accLEDPort();
  //delay(WAIT);
  Serial.println("Go run blinkuserLED");
  blinkUserLEDRed();   // end of cycle - Go loop
  blinkUserLEDGreen();
  */
 // displayText(4, "go loop");
  Serial.println("Go Loop");
}