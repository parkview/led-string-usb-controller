EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title "LED String Controller - UART"
Date "2020-10-14"
Rev "1.1"
Comp "PRL"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Interface_USB:CP2102N-A01-GQFN28 U?
U 1 1 5EE7A5F2
P 6085 4490
AR Path="/5EE7A5F2" Ref="U?"  Part="1" 
AR Path="/5EE62BCC/5EE7A5F2" Ref="U?"  Part="1" 
AR Path="/5FEA426B/5EE7A5F2" Ref="U8"  Part="1" 
F 0 "U8" H 5800 3230 50  0000 C CNN
F 1 "CP2102N-A01-GQFN28" H 6600 3210 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-28-1EP_5x5mm_P0.5mm_EP3.35x3.35mm" H 6535 3290 50  0001 L CNN
F 3 "https://www.silabs.com/documents/public/data-sheets/cp2102n-datasheet.pdf" H 6135 3740 50  0001 C CNN
	1    6085 4490
	1    0    0    -1  
$EndComp
Wire Wire Line
	4830 3990 5065 3990
Wire Wire Line
	5065 3990 5065 3890
Wire Wire Line
	5065 3890 5585 3890
Wire Wire Line
	4830 4090 5190 4090
Wire Wire Line
	5190 4090 5190 3990
Wire Wire Line
	5190 3990 5585 3990
$Comp
L power:GND #PWR054
U 1 1 5EE7E8E5
P 4530 4480
F 0 "#PWR054" H 4530 4230 50  0001 C CNN
F 1 "GND" H 4535 4307 50  0000 C CNN
F 2 "" H 4530 4480 50  0001 C CNN
F 3 "" H 4530 4480 50  0001 C CNN
	1    4530 4480
	1    0    0    -1  
$EndComp
Wire Wire Line
	4530 4390 4530 4435
Wire Wire Line
	4430 4390 4430 4435
Wire Wire Line
	4430 4435 4530 4435
Connection ~ 4530 4435
Wire Wire Line
	4530 4435 4530 4480
NoConn ~ 5585 3590
Text GLabel 7065 3590 2    50   Input ~ 0
TxD
Text GLabel 7070 3690 2    50   Input ~ 0
RxD
Wire Wire Line
	6585 3590 6785 3590
NoConn ~ 6585 5390
NoConn ~ 6585 5290
NoConn ~ 6585 5190
NoConn ~ 6585 5090
NoConn ~ 6585 4990
NoConn ~ 6585 5490
NoConn ~ 6585 5590
NoConn ~ 6585 4390
NoConn ~ 6585 4590
NoConn ~ 6585 4790
NoConn ~ 6585 4690
$Comp
L power:GND #PWR058
U 1 1 5EE816CF
P 6085 5845
F 0 "#PWR058" H 6085 5595 50  0001 C CNN
F 1 "GND" H 6090 5672 50  0000 C CNN
F 2 "" H 6085 5845 50  0001 C CNN
F 3 "" H 6085 5845 50  0001 C CNN
	1    6085 5845
	1    0    0    -1  
$EndComp
Wire Wire Line
	6085 5790 6085 5845
$Comp
L power:+3V3 #PWR057
U 1 1 5EE82456
P 6085 3035
F 0 "#PWR057" H 6085 2885 50  0001 C CNN
F 1 "+3V3" H 6100 3208 50  0000 C CNN
F 2 "" H 6085 3035 50  0001 C CNN
F 3 "" H 6085 3035 50  0001 C CNN
	1    6085 3035
	1    0    0    -1  
$EndComp
Wire Wire Line
	6085 3190 6085 3120
Wire Wire Line
	5985 3190 5985 3120
Wire Wire Line
	5985 3120 6085 3120
Connection ~ 6085 3120
Wire Wire Line
	6085 3120 6085 3035
$Comp
L Device:C_Small C27
U 1 1 5EE833DE
P 5780 3120
F 0 "C27" V 5730 3210 50  0000 C CNN
F 1 "10uF" V 5855 2980 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5780 3120 50  0001 C CNN
F 3 "~" H 5780 3120 50  0001 C CNN
	1    5780 3120
	0    1    1    0   
$EndComp
Wire Wire Line
	5985 3120 5880 3120
Connection ~ 5985 3120
$Comp
L Device:C_Small C26
U 1 1 5EE84268
P 5780 2925
F 0 "C26" V 5725 3015 50  0000 C CNN
F 1 "100nF" V 5730 2770 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5780 2925 50  0001 C CNN
F 3 "~" H 5780 2925 50  0001 C CNN
	1    5780 2925
	0    1    1    0   
$EndComp
Wire Wire Line
	5880 2925 5985 2925
Wire Wire Line
	5985 2925 5985 3120
$Comp
L power:GND #PWR056
U 1 1 5EE84AA7
P 5460 3175
F 0 "#PWR056" H 5460 2925 50  0001 C CNN
F 1 "GND" H 5465 3002 50  0000 C CNN
F 2 "" H 5460 3175 50  0001 C CNN
F 3 "" H 5460 3175 50  0001 C CNN
	1    5460 3175
	1    0    0    -1  
$EndComp
Wire Wire Line
	5680 3120 5610 3120
Wire Wire Line
	5460 3120 5460 3175
Wire Wire Line
	5680 2925 5610 2925
Wire Wire Line
	5610 2925 5610 3120
Connection ~ 5610 3120
Wire Wire Line
	5610 3120 5460 3120
$Comp
L Device:C_Small C25
U 1 1 5EE8933C
P 5155 3230
F 0 "C25" V 5105 3320 50  0000 C CNN
F 1 "10uF" V 5195 3105 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5155 3230 50  0001 C CNN
F 3 "~" H 5155 3230 50  0001 C CNN
	1    5155 3230
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C24
U 1 1 5EE89346
P 5155 3035
F 0 "C24" V 5100 3125 50  0000 C CNN
F 1 "100nF" V 5105 2880 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 5155 3035 50  0001 C CNN
F 3 "~" H 5155 3035 50  0001 C CNN
	1    5155 3035
	0    1    1    0   
$EndComp
Wire Wire Line
	5055 3230 4985 3230
Wire Wire Line
	5055 3035 4985 3035
Wire Wire Line
	4985 3035 4985 3230
Wire Wire Line
	5255 3230 5255 3120
Wire Wire Line
	5255 3120 5330 3120
Connection ~ 5460 3120
Text Notes 7505 5495 0    50   ~ 0
Auto Program:\nDTR     RTS  -->  EN    IO0\n 1        1          1      1\n 0        0          1      1\n 1        0          0      1\n 0        1          1      0
$Comp
L Device:R_Small R20
U 1 1 5EE8C331
P 8165 4110
F 0 "R20" V 7969 4110 50  0000 C CNN
F 1 "10K" V 8060 4110 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8165 4110 50  0001 C CNN
F 3 "~" H 8165 4110 50  0001 C CNN
	1    8165 4110
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R21
U 1 1 5EE8F523
P 8175 4735
F 0 "R21" V 7979 4735 50  0000 C CNN
F 1 "10K" V 8070 4735 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 8175 4735 50  0001 C CNN
F 3 "~" H 8175 4735 50  0001 C CNN
	1    8175 4735
	0    1    1    0   
$EndComp
Wire Wire Line
	8685 4310 8685 4405
Wire Wire Line
	8685 4405 7920 4405
Wire Wire Line
	7920 4405 7920 4735
Wire Wire Line
	7920 4735 8075 4735
Wire Wire Line
	8685 4535 8685 4470
Wire Wire Line
	8685 4470 7865 4470
Wire Wire Line
	7865 4470 7865 4110
Wire Wire Line
	7865 4110 8065 4110
Wire Wire Line
	7865 4110 7735 4110
Connection ~ 7865 4110
Wire Wire Line
	7920 4735 7775 4735
Connection ~ 7920 4735
Wire Wire Line
	8685 4935 8685 5000
Wire Wire Line
	8685 5000 8995 5000
Text GLabel 8970 3845 2    50   Input ~ 0
EN
Text GLabel 8995 5000 2    50   Input ~ 0
IO0
Text GLabel 7775 4735 0    50   Input ~ 0
RTS
Text GLabel 7735 4110 0    50   Input ~ 0
DTR
Text GLabel 6700 3890 2    50   Input ~ 0
DTR
Text GLabel 6700 3490 2    50   Input ~ 0
RTS
Wire Wire Line
	6585 3490 6700 3490
Wire Wire Line
	6585 3890 6700 3890
Text GLabel 6700 3390 2    50   Input ~ 0
CTS
NoConn ~ 6585 3790
NoConn ~ 6585 3990
NoConn ~ 6585 4090
Wire Wire Line
	8685 3910 8685 3845
Wire Wire Line
	8685 3845 8970 3845
$Comp
L Pauls_Symbol_Library:SS8050 Q4
U 1 1 5EFB2A1D
P 8635 4110
F 0 "Q4" H 8775 4156 50  0000 L CNN
F 1 "SS8050" H 8775 4065 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8435 3560 50  0001 C CNN
F 3 "" H 8435 3560 50  0001 C CNN
	1    8635 4110
	1    0    0    -1  
$EndComp
Wire Wire Line
	8265 4110 8435 4110
$Comp
L Pauls_Symbol_Library:SS8050 Q5
U 1 1 5EFBC135
P 8635 4735
F 0 "Q5" H 8775 4781 50  0000 L CNN
F 1 "SS8050" H 8775 4690 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8435 4185 50  0001 C CNN
F 3 "" H 8435 4185 50  0001 C CNN
	1    8635 4735
	1    0    0    1   
$EndComp
Wire Wire Line
	8275 4735 8435 4735
$Comp
L Device:R_Small R18
U 1 1 5EA60FC6
P 5005 3790
F 0 "R18" V 4809 3790 50  0000 C CNN
F 1 "24K" V 4900 3790 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5005 3790 50  0001 C CNN
F 3 "~" H 5005 3790 50  0001 C CNN
	1    5005 3790
	0    1    1    0   
$EndComp
$Comp
L Device:R_Small R19
U 1 1 5EA61456
P 5330 3620
F 0 "R19" V 5134 3620 50  0000 C CNN
F 1 "47K" V 5225 3620 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 5330 3620 50  0001 C CNN
F 3 "~" H 5330 3620 50  0001 C CNN
	1    5330 3620
	-1   0    0    1   
$EndComp
Wire Wire Line
	4830 3790 4905 3790
Wire Wire Line
	5105 3790 5200 3790
Connection ~ 5330 3790
Wire Wire Line
	5330 3790 5585 3790
Wire Wire Line
	5330 3720 5330 3790
Wire Wire Line
	5330 3520 5330 3120
Connection ~ 5330 3120
Wire Wire Line
	5330 3120 5460 3120
Connection ~ 5255 3120
Wire Wire Line
	5255 3120 5255 3035
Wire Wire Line
	4985 3230 4985 3395
Wire Wire Line
	4985 3395 5200 3395
Wire Wire Line
	5200 3395 5200 3790
Connection ~ 4985 3230
Connection ~ 5200 3790
Wire Wire Line
	5200 3790 5330 3790
NoConn ~ 6585 4290
Wire Wire Line
	6585 3390 6700 3390
$Comp
L power:VBUS #PWR055
U 1 1 60217A10
P 4830 3565
F 0 "#PWR055" H 4830 3415 50  0001 C CNN
F 1 "VBUS" H 4845 3738 50  0000 C CNN
F 2 "" H 4830 3565 50  0001 C CNN
F 3 "" H 4830 3565 50  0001 C CNN
	1    4830 3565
	1    0    0    -1  
$EndComp
Wire Wire Line
	4830 3790 4830 3565
Wire Wire Line
	6585 3690 6785 3690
$Comp
L Device:Jumper_NC_Small JP5
U 1 1 60033C77
P 6885 3690
F 0 "JP5" H 6995 3750 50  0000 C CNN
F 1 "Jumper_NC_Small" H 6885 3811 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6885 3690 50  0001 C CNN
F 3 "~" H 6885 3690 50  0001 C CNN
	1    6885 3690
	1    0    0    -1  
$EndComp
Wire Wire Line
	6985 3690 7070 3690
$Comp
L Device:Jumper_NC_Small JP4
U 1 1 600346D6
P 6885 3590
F 0 "JP4" H 7000 3650 50  0000 C CNN
F 1 "Jumper_NC_Small" H 6885 3711 50  0001 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric" H 6885 3590 50  0001 C CNN
F 3 "~" H 6885 3590 50  0001 C CNN
	1    6885 3590
	1    0    0    -1  
$EndComp
Wire Wire Line
	6985 3590 7065 3590
Text Notes 6730 3805 0    50   ~ 0
isolation jumpers
$Comp
L Connector:USB_B_Micro J10
U 1 1 5F9DBEB4
P 4530 3990
F 0 "J10" H 4587 4457 50  0000 C CNN
F 1 "USB_B_Micro" H 4587 4366 50  0000 C CNN
F 2 "Connector_USB:USB_Micro-B_Amphenol_10103594-0001LF_Horizontal" H 4680 3940 50  0001 C CNN
F 3 "~" H 4680 3940 50  0001 C CNN
	1    4530 3990
	1    0    0    -1  
$EndComp
Connection ~ 4830 3790
Wire Wire Line
	4530 4435 4830 4435
Wire Wire Line
	4830 4435 4830 4190
$EndSCHEMATC
